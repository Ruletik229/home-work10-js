let a = '', b = '', count = 0, finish = false,key = '', sign = '', m = '';

          window.addEventListener("DOMContentLoaded", ()=>{
            const btn = document.querySelector(".keys"),
            display = document.querySelector(".display > input"),
            span1 = document.createElement("span");
            span1.id = "span1-style";
            document.querySelector(".display").append(span1);
            rez = document.querySelector(".rez");
            dis = document.querySelector(".dis")

            btn.addEventListener("click", function (e) {
              document.querySelector(".c").onclick = (e) =>{
                a = '';
                b = '';
                finish = false;
                sign = '';
                span1.textContent = '';
                display.value = 0;
                rez.setAttribute('disabled', '');
                dis.setAttribute('disabled', '');
              }

              if(e.target.classList.contains("black")){
                key = e.target.value;
                if(b === '' && sign === ''){
                  a += key;
                  display.value = a;
                }else if(a!== '' && b!== '' && finish){
                  b = key;
                  finish = false;
                  display.value = b;
                }else{
                  b += key;
                  display.value = b;
                  rez.removeAttribute('disabled');
                }
              }

             if(e.target.classList.contains("pink")){
              sign = e.target.value;
              display.value.hidden = sign;
              return;
             }

             if(e.target.classList.contains("orange")){
              if(e.target.value === '='){
                if(b === '') b = a;
                switch(sign){
                case "+":
                  a = (+a) + (+b);
                  break;
                case "-":
                  a = a - b;
                  break;
                case "*":
                  a = a * b;
                  break;
                case "/":
                  a = a / b;
                  break;
              }
              finish = true;
              display.value = a;
              }
             }

             if(e.target.classList.contains("gray")){
              if(e.target.value ==="m+"){
                span1.textContent = "m";
                dis.removeAttribute("disabled");
                m = m + a;
              }

              if(e.target.value === "m-"){
                span1.textContent = "m";
                dis.removeAttribute("disabled");
                m = m - a;
              }

              if(e.target.value === "mrc"){
                display.value = m;
                count++;
                if(count === 2){
                  m = 0;
                  display.value = 0;
                  span1.textContent = '';
                  count = 0;
                  finish = false;
                }
              }
             }
            })
          })
    